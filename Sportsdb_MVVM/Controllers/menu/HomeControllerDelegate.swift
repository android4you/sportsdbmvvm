//
//  HomeControllerDelegate.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 22/04/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation

protocol HomeControllerDelegate {
    func handleMenuToggle(forMenuOption menuOption: Int?)
}
