//
//  MenuViewController.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 22/04/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    
    var delegate: HomeControllerDelegate?
    @IBOutlet weak var tableview: UITableView!
    var models = [MenuModel]()
    let cellId = "MenuCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.register(UINib.init(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorColor = UIColor.clear
        tableview.layoutIfNeeded()
        models = menuItems()
        tableview.reloadData()
    }
    
    func  menuItems() -> [MenuModel] {
        var models = [MenuModel]()
        models.append(MenuModel(id: 1, title: "Sports", description: "", image: "wallet.png"))
        models.append(MenuModel(id: 2, title: "Leagues", description: "", image: "notification.png"))
        models.append(MenuModel(id: 3, title: "Countries", description: "", image: "profile"))
        models.append(MenuModel(id: 4, title: "Teams", description: "", image: "map.png"))
        models.append(MenuModel(id: 5, title: "Loved One", description: "", image: "support.png"))
        return models
    }
}

extension MenuViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MenuCell
        cell.selectionStyle = .none
        let menu = models[indexPath.row]
        cell.menuTitle.text = menu.title
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
       
        delegate?.handleMenuToggle(forMenuOption: indexPath.row)
    }
    
    
    
}

