//
//  TeamsViewController.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 31/07/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class TeamsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.barStyle = .black
        
        navigationItem.title = "Teams"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image:
                 UIImage(named:"back_icon")?.withRenderingMode(.alwaysOriginal) , style: .plain, target: self, action: #selector(backButtonToggle))
        }


        @objc func backButtonToggle(){
           navigationController?.popViewController(animated: true)
        }
         

}
