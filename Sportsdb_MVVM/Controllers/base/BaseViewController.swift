//
//  BaseViewController.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 02/08/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    func configureBackNavigationBar(title: String){
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.barStyle = .black
        navigationItem.title = title
        navigationItem.leftBarButtonItem = UIBarButtonItem(image:
            UIImage(named:"back_icon")?.withRenderingMode(.alwaysOriginal) , style: .plain, target: self, action: #selector(backButtonToggle))
    }
    @objc func backButtonToggle(){
        navigationController?.popViewController(animated: true)
    }
    
}
