//
//  ProfileViewController.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 26/05/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    
    var delegate: HomeControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        //configureNavigationBar()
    }
    
    
    @objc func handleMenuToggle(){
        delegate?.handleMenuToggle(forMenuOption: nil)
    }
    
    func configureNavigationBar(){
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.barStyle = .black
        
        navigationItem.title = "Menu"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image:
            #imageLiteral(resourceName: "menu").withRenderingMode(.alwaysOriginal) , style: .plain, target: self, action: #selector(handleMenuToggle))
    }


}
