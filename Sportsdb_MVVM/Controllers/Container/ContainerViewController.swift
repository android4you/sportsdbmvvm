//
//  ContainerViewController.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 21/04/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.

    import UIKit

    class ContainerViewController: UIViewController {
        
        var menuController : MenuViewController!
        var centerController : UIViewController!
        var isExpanded = false
        var homeController: HomeViewController!
        
        override func viewDidLoad() {
            super.viewDidLoad()
            configureHomeController()
        }
        
        override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation{
            return .slide
        }
        
        override var prefersStatusBarHidden: Bool{
            return isExpanded
        }
        
        func configureHomeController() {
            homeController = HomeViewController()
            homeController.delegate = self
            centerController = UINavigationController(rootViewController: homeController)
            view.addSubview(centerController.view)
         //   addChild(centerController)
            centerController.didMove(toParent: self)
            
        }
        
        func configureMenuController(){
            if menuController == nil {
                menuController = MenuViewController()
                menuController.delegate = self
                view.insertSubview(menuController.view, at: 0)
                addChild(menuController)
                menuController.didMove(toParent: self)
            }
        }
        
        func animatePanel(shouldExpand: Bool, menuOption: Int?){
            
            if shouldExpand{
                // show menu
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                    self.centerController.view.frame.origin.x = self.centerController.view.frame.width - 80
                    
                }, completion: nil)
            }else{
                // hide menu
                UIView.animate(withDuration: 0.9, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                        self.centerController.view.frame.origin.x = 0

                }) { (_) in
                    guard let menuOption = menuOption else {return}
                    self.didSelectMenuOption(menuOption: menuOption)
                }
            }

            animateStatusBar()
        }
        
        func didSelectMenuOption(menuOption: Int){
            switch menuOption {
            case 1:
                homeController.navigationController?.pushViewController(LeagueViewController(), animated: true)
               
            case 2:
                homeController.navigationController?.pushViewController(CountryViewController(), animated: true)
            case 3:
                homeController.navigationController?.pushViewController(TeamsViewController(), animated: true)
            case 4:
                 homeController.navigationController?.pushViewController(LeagueViewController(), animated: true)

            default:
                 print("show setting")
            }
           }
        
        func animateStatusBar(){
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                self.setNeedsStatusBarAppearanceUpdate()
            }, completion: nil)

        }
    }

    extension ContainerViewController: HomeControllerDelegate {
        
        func handleMenuToggle(forMenuOption menuOption: Int?) {
            if !isExpanded{
                configureMenuController()
            }
            
            isExpanded = !isExpanded
            animatePanel(shouldExpand: isExpanded, menuOption: menuOption)
        }
        
    }

