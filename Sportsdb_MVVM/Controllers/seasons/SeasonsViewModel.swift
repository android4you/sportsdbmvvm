//
//  SeasonsViewModel.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 02/08/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation
import RxSwift

class SeasonsViewModel  {
    
    let disposeBag = DisposeBag()
    
    let seasonRepository = SeasonRepository()
    
    let seasonResponse = PublishSubject<SeasonsListResponse>()
    
    var seasonList: [SeasonsModel] = []
    
    
    func getSeasonList() {
        
        seasonRepository
            .getSeasonList().subscribe(onNext: { [weak self] (seasonsListResponse) in
                
                if let seasonInfoList = seasonsListResponse.seasons {
                    self?.seasonList = seasonInfoList
                }
                self?.seasonResponse.onNext(seasonsListResponse)
                
                }, onError: { [weak self] (error) in
                    self?.seasonResponse.onError(error)
            })
            .disposed(by: disposeBag)
    }
    
}
