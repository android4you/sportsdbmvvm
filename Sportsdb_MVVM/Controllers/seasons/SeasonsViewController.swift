//
//  SeasonsViewController.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 31/07/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class SeasonsViewController: BaseViewController {
    
    
    let viewModel = SeasonsViewModel()
    
    let cellId = "SeasonsTableViewCell"
    
    var sentData: String?
    
    @IBOutlet weak var seasonsListView: UITableView!
    
    
    @IBOutlet weak var spinnerView: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureBackNavigationBar(title: "Seasons")
        
        seasonsListView.register(UINib.init(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        seasonsListView.delegate = self
        seasonsListView.dataSource = self
        seasonsListView.separatorColor = UIColor.clear
        seasonsListView.translatesAutoresizingMaskIntoConstraints = false;
        seasonsListView.rowHeight = 70
        seasonsListView.layoutIfNeeded()
        
        spinnerView.startAnimating()
        subscribeLeagueResponse()
        viewModel.getSeasonList()
    }
    
    
    func subscribeLeagueResponse() {
        viewModel.seasonResponse.subscribe(onNext: { [weak self] (seasonResponse) in
            if(seasonResponse.seasons?.count ?? 6 > 0){
                self?.seasonsListView.reloadData()
                self?.spinnerView.stopAnimating()
                self?.spinnerView.isHidden = true
            }
            }, onError: { (error) in
                print(error.localizedDescription)
                self.spinnerView.stopAnimating()
                self.spinnerView.isHidden = true
        })
            .disposed(by: viewModel.disposeBag)
    }
    
    
}


extension SeasonsViewController: UITableViewDelegate, UITableViewDataSource   {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.seasonList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SeasonsTableViewCell
        cell.selectionStyle = .none
        cell.titleView.text = viewModel.seasonList[indexPath.row].strSeason
        return cell
    }
}
