//
//  LeagueViewController.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 31/07/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class LeagueViewController: BaseViewController {
    
    let viewModel = LeagueViewModel()
    
    @IBOutlet weak var leagueListView: UITableView!
    
    @IBOutlet weak var spinnerView: UIActivityIndicatorView!
    
    
    let cellId = "LeagueTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureBackNavigationBar(title: "Leagues")
        
        leagueListView.register(UINib.init(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        leagueListView.delegate = self
        leagueListView.dataSource = self
        leagueListView.separatorColor = UIColor.clear
        leagueListView.translatesAutoresizingMaskIntoConstraints = false;
        leagueListView.rowHeight = 90
        leagueListView.layoutIfNeeded()
        
        spinnerView.startAnimating()
        subscribeLeagueResponse()
        viewModel.getLeaguesList()
        
    }
    
    
    func subscribeLeagueResponse() {
        viewModel.leagueListResponse.subscribe(onNext: { [weak self] (leagueResponse) in
            if(leagueResponse.leagues?.count ?? 6 > 0){
                self?.leagueListView.reloadData()
                self?.spinnerView.stopAnimating()
                self?.spinnerView.isHidden = true
               //    self.activityIndicator.hidesWhenStopped = true
            }
            }, onError: { (error) in
                 print(error.localizedDescription)
                self.spinnerView.stopAnimating()
                self.spinnerView.isHidden = true
        })
            .disposed(by: viewModel.disposeBag)
    }
    
}


extension LeagueViewController: UITableViewDelegate, UITableViewDataSource   {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.leagueList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! LeagueTableViewCell
        cell.selectionStyle = .none
        var combined3 = viewModel.leagueList[indexPath.row].strSport!
        combined3 += " - "
        combined3 += viewModel.leagueList[indexPath.row].strLeague!
        cell.titleView.text = combined3
        
        
        return cell
    }
}
