//
//  LeagueViewModel.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 01/08/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation
import RxSwift

class LeagueViewModel {
    let disposeBag = DisposeBag()
    
    let leagueRepository = LeagueRepository()
    
    let leagueListResponse = PublishSubject<LeagueListResponse>()
    
    var leagueList: [LeagueModel] = []
    
    
    func getLeaguesList() {
        
        leagueRepository
            .getLeaguesList().subscribe(onNext: { [weak self] (leagueListResponse) in
                
                if let leaguesInfoList = leagueListResponse.leagues {
                    self?.leagueList = leaguesInfoList
                }
                self?.leagueListResponse.onNext(leagueListResponse)
                
                }, onError: { [weak self] (error) in
                    self?.leagueListResponse.onError(error)
            })
            .disposed(by: disposeBag)
    }

    
}



