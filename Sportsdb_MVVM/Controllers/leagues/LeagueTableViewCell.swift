//
//  LeagueTableViewCell.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 02/08/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class LeagueTableViewCell: UITableViewCell {

    @IBOutlet weak var titleView: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
