//
//  CountryViewController.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 01/08/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class CountryViewController: BaseViewController {
    
    let viewModel = CountryViewModel()
    
    let cellId = "CountryTableViewCell"
    
    @IBOutlet weak var countryListView: UITableView!
    
    @IBOutlet weak var spinnerView: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureBackNavigationBar(title: "Countries")
        
        countryListView.register(UINib.init(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        countryListView.delegate = self
        countryListView.dataSource = self
        countryListView.separatorColor = UIColor.clear
        countryListView.translatesAutoresizingMaskIntoConstraints = false;
        countryListView.rowHeight = 70
        countryListView.layoutIfNeeded()
        spinnerView.startAnimating()
        subscribeCountriesResponse()
        viewModel.getCountriesList()
    }
    
    func subscribeCountriesResponse() {
        viewModel.countriesResponse.subscribe(onNext: { [weak self] (countryResponse) in
            if(countryResponse.countries?.count ?? 6 > 0){
                self?.countryListView.reloadData()
                self?.spinnerView.stopAnimating()
                self?.spinnerView.isHidden = true
            }
            }, onError: { (error) in
                print(error.localizedDescription)
                self.spinnerView.stopAnimating()
                self.spinnerView.isHidden = true
        })
            .disposed(by: viewModel.disposeBag)
    }
    
}


extension CountryViewController: UITableViewDelegate, UITableViewDataSource   {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.countryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! CountryTableViewCell
        cell.selectionStyle = .none
        cell.titleView.text = viewModel.countryList[indexPath.row].name_en
        return cell
    }
}
