//
//  CountryViewModel.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 01/08/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation
import RxSwift

class CountryViewModel  {
    
    let disposeBag = DisposeBag()
    
    let countryRepository = CountryRepository()
    
    let countriesResponse = PublishSubject<CountriesResponse>()
    
    var countryList: [CountryModel] = []
    
    
    func getCountriesList() {
        
        countryRepository
            .getCountryList().subscribe(onNext: { [weak self] (countriesListResponse) in
                
                if let countryInfoList = countriesListResponse.countries {
                    self?.countryList = countryInfoList
                }
                self?.countriesResponse.onNext(countriesListResponse)
                
                }, onError: { [weak self] (error) in
                    self?.countriesResponse.onError(error)
            })
            .disposed(by: disposeBag)
    }
    
}
