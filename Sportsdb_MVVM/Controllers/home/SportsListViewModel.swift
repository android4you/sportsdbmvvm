//
//  SportsListViewModel.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 01/08/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//
import Foundation
import RxSwift

class SportsListViewModel {
    
    let disposeBag = DisposeBag()
    
    let sportsListRepository = SportsListRepository()
    
    let sportsListResponse = PublishSubject<SportsListResponse>()
    
    var sportsList: [SportsModel] = []
    
    
    func fetchSportsList() {
        
        sportsListRepository
            .getSportsList().subscribe(onNext: { [weak self] (sportsListResponse) in
                
                if let sportsInfoList = sportsListResponse.sports {
                    self?.sportsList = sportsInfoList
                }
                self?.sportsListResponse.onNext(sportsListResponse)
                
                }, onError: { [weak self] (error) in
                    self?.sportsListResponse.onError(error)
            })
            .disposed(by: disposeBag)
    }
    
    
}


