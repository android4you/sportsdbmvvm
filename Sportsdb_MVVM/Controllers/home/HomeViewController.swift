//
//  HomeViewController.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 22/04/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {
    
    var delegate: HomeControllerDelegate?
    
    let viewModel = SportsListViewModel()
    
    @IBOutlet weak var sportListView: UITableView!
    
    @IBOutlet weak var spinnerView: UIActivityIndicatorView!
    
    
    let cellId = "SportsTableViewCell"
    
    var imageLoader = ImageLoader()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        
        sportListView.register(UINib.init(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        sportListView.delegate = self
        sportListView.dataSource = self
        sportListView.separatorColor = UIColor.clear
        sportListView.translatesAutoresizingMaskIntoConstraints = false;
        sportListView.rowHeight = 70
        sportListView.layoutIfNeeded()
        spinnerView.startAnimating()
        subscribeSportsResponse()
        viewModel.fetchSportsList()
    }
    
    func subscribeSportsResponse() {
        viewModel.sportsListResponse.subscribe(onNext: { [weak self] (sportsResponse) in
            if(sportsResponse.sports?.count ?? 6 > 0){
                self?.sportListView.reloadData()
                self?.spinnerView.stopAnimating()
                self?.spinnerView.isHidden = true
            }
            }, onError: { (error) in
                print(error.localizedDescription)
                self.spinnerView.stopAnimating()
                self.spinnerView.isHidden = true
        })
            .disposed(by: viewModel.disposeBag)
    }
    
    @objc func handleMenuToggle(){
        
        delegate?.handleMenuToggle(forMenuOption: nil)
    }
    
    func configureNavigationBar(){
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.barStyle = .black
        navigationItem.title = "Sports"
        navigationItem.leftBarButtonItem = UIBarButtonItem(image:
            #imageLiteral(resourceName: "menu").withRenderingMode(.alwaysOriginal) , style: .plain, target: self, action: #selector(handleMenuToggle))
    }
}


extension HomeViewController: UITableViewDelegate, UITableViewDataSource   {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.sportsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SportsTableViewCell
         cell.selectionStyle = .none
        if let sportsModel = viewModel.sportsList[indexPath.row] as? SportsModel {
            cell.titleView.text = sportsModel.strSport
            if let imageUrl = sportsModel.strSportThumb{
                let str = String(describing: URL(string: imageUrl)!)
                cell.iconView.downloaded(from: str)
            }
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           print(indexPath.row)
        let seasonsViewController = SeasonsViewController()
        seasonsViewController.sentData = "Manu "
          navigationController?.pushViewController(seasonsViewController, animated: true)
     
       }
       
}
