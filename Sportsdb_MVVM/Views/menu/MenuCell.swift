//
//  MenuCell.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 23/04/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {

    @IBOutlet weak var menuTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
