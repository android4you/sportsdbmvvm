//
//  SeasonRepository.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 02/08/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation
import RxSwift

class SeasonRepository {
    func getSeasonList() -> Observable<SeasonsListResponse> {
         return Observable.create({ observer -> Disposable in
             
            AlamofireService.getSeasonList(params: "4328", path: URLPath.listSeasons, completion: { (response) in
                 
                 if let error = response.error {
                     print("‼️ Failed. (getSeasonList) *-> Error: ", error.localizedDescription)
                     observer.onError(error)
                     return
                 }
                
                observer.onNext(response.result.value!)
                 observer.onCompleted()
             })
             return Disposables.create()
         })
     }
     
}
