//
//  CountryRepository.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 01/08/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//


import Foundation
import RxSwift

class CountryRepository {
    func getCountryList() -> Observable<CountriesResponse> {
         return Observable.create({ observer -> Disposable in
             
            AlamofireService.getCountriesRList(path: URLPath.listCountries, completion: { (response) in
                 
                 if let error = response.error {
                     print("‼️ Failed. (getLeaguesList) *-> Error: ", error.localizedDescription)
                     observer.onError(error)
                     return
                 }
                
                observer.onNext(response.result.value!)
                 observer.onCompleted()
             })
             return Disposables.create()
         })
     }
     
}

