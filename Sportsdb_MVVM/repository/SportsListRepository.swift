//
//  SportsListRepository.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 01/08/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation
import RxSwift

class SportsListRepository {
    func getSportsList() -> Observable<SportsListResponse> {
         return Observable.create({ observer -> Disposable in
             
            AlamofireService.getSportsList(path: URLPath.listSports, completion: { (response) in
                 
                 if let error = response.error {
                     print("‼️ Failed. (getSportsList) *-> Error: ", error.localizedDescription)
                     observer.onError(error)
                     return
                 }
                
                observer.onNext(response.result.value!)
                 observer.onCompleted()
             })
             return Disposables.create()
         })
     }
     
}
