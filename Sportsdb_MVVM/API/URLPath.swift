//
//  URLPath.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 31/07/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation

struct URLPath {
    
    static let listSports = "/api/v1/json/1/all_sports.php"
    
    static let listLeagues = "/api/v1/json/1/all_leagues.php"
    
    static let listCountries = "/api/v1/json/1/all_countries.php"
    
    static let listSeasons = "/api/v1/json/1/search_all_seasons.php"
    
    static let listOfAllLeagues = "/api/v1/json/1/search_all_leagues.php"
    
   // ortsdb.com/api/v1/json/1/search_all_leagues.php?c=England
    
}
