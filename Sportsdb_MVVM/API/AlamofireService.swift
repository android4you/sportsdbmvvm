//
//  AlamofireService.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 31/07/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//


import Alamofire
import AlamofireObjectMapper

class AlamofireService {
    
    static let defaultHeaderParams = [
        "accept": "application/json"
    ]
    
    static var urlScheme: String {
        get {
            return "https"
        }
    }
    
    static func getSportsList( path: String, completion: @escaping (DataResponse<SportsListResponse>) -> ()) {
        
        var urlComponents = URLComponents()
        urlComponents.host = ApplicationConfig.host
        urlComponents.path = path
        urlComponents.scheme = urlScheme
        
        
        print("\n\(urlComponents.string!) ")
        
        
        Alamofire.request(urlComponents.string!, method: .get, encoding: URLEncoding.default, headers: defaultHeaderParams)
            .validate(statusCode: 200..<600)
            .responseObject { (response: DataResponse<SportsListResponse>) in
                
                completion(response)
        }
    }
    
    
    static func getLeaguesList( path: String, completion: @escaping (DataResponse<LeagueListResponse>) -> ()) {
        
        var urlComponents = URLComponents()
        urlComponents.host = ApplicationConfig.host
        urlComponents.path = path
        urlComponents.scheme = urlScheme
        
        
        print("\n\(urlComponents.string!) ")
        
        
        Alamofire.request(urlComponents.string!, method: .get, encoding: URLEncoding.default, headers: defaultHeaderParams)
            .validate(statusCode: 200..<600)
            .responseObject { (response: DataResponse<LeagueListResponse>) in
                
                completion(response)
        }
    }
    
 
    
    static func getCountriesRList( path: String, completion: @escaping (DataResponse<CountriesResponse>) -> ()) {
         
         var urlComponents = URLComponents()
         urlComponents.host = ApplicationConfig.host
         urlComponents.path = path
         urlComponents.scheme = urlScheme
         
         
         print("\n\(urlComponents.string!) ")
         
         
         Alamofire.request(urlComponents.string!, method: .get, encoding: URLEncoding.default, headers: defaultHeaderParams)
             .validate(statusCode: 200..<600)
             .responseObject { (response: DataResponse<CountriesResponse>) in
                 
                 completion(response)
         }
     }
    
    
//
//
//    static func getSeasonList( path: String, completion: @escaping (DataResponse<SeasonsListResponse>) -> ()) {
//
//         var urlComponents = URLComponents()
//         urlComponents.host = ApplicationConfig.host
//         urlComponents.path = path
//         urlComponents.scheme = urlScheme
//
//
//         print("\n\(urlComponents.string!) ")
//
//
//         Alamofire.request(urlComponents.string!, method: .get, encoding: URLEncoding.default, headers: defaultHeaderParams)
//             .validate(statusCode: 200..<600)
//             .responseObject { (response: DataResponse<SeasonsListResponse>) in
//
//                 print(response.result.value ?? "")
//                 completion(response)
//         }
//     }
     
    
     static func getSeasonList(params: String, path: String, completion: @escaping (DataResponse<SeasonsListResponse>) -> ()) {
            
            var urlComponents = URLComponents()
            urlComponents.host = ApplicationConfig.host
            urlComponents.path = path
            urlComponents.scheme = urlScheme
            
            urlComponents.queryItems = [
                URLQueryItem(name: "id", value: params)
            ]
            
        
            
            print("\n\(urlComponents.string!) istek atılıyor...")
            Alamofire.request(urlComponents.string!, method: .get, encoding: URLEncoding.default, headers: defaultHeaderParams)
                .validate(statusCode: 200..<600)
                .responseObject { (response: DataResponse<SeasonsListResponse>) in
                completion(response)
            }
    }
        
    
    static func getAllLeagueCountryList(params: String, path: String, completion: @escaping (DataResponse<AllLeagueCountryResponse>) -> ()) {
             
             var urlComponents = URLComponents()
             urlComponents.host = ApplicationConfig.host
             urlComponents.path = path
             urlComponents.scheme = urlScheme
             
             urlComponents.queryItems = [
                 URLQueryItem(name: "c", value: params)
             ]
             
         
             
             print("\n\(urlComponents.string!) istek atılıyor...")
             Alamofire.request(urlComponents.string!, method: .get, encoding: URLEncoding.default, headers: defaultHeaderParams)
                 .validate(statusCode: 200..<600)
                 .responseObject { (response: DataResponse<AllLeagueCountryResponse>) in
                 completion(response)
             }
     }
    
}
