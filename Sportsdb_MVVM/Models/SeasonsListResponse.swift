//
//  SeasonsListResponse.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 02/08/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation
import ObjectMapper

struct SeasonsListResponse : Mappable {
    var seasons : [SeasonsModel]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        seasons <- map["seasons"]
    }

}
