//
//  CountrysModel.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 03/08/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//
import Foundation
import ObjectMapper

struct CountrysModel : Mappable {
    var idLeague : String?
    var idSoccerXML : String?
    var idAPIfootball : String?
    var strSport : String?
    var strLeague : String?
    var strLeagueAlternate : String?
    var strDivision : String?
    var idCup : String?
    var strCurrentSeason : String?
    var intFormedYear : String?
    var dateFirstEvent : String?
    var strGender : String?
    var strCountry : String?
    var strWebsite : String?
    var strFacebook : String?
    var strTwitter : String?
    var strYoutube : String?
    var strRSS : String?
    var strDescriptionEN : String?
    var strDescriptionDE : String?
    var strDescriptionFR : String?
    var strDescriptionIT : String?
    var strDescriptionCN : String?
    var strDescriptionJP : String?
    var strDescriptionRU : String?
    var strDescriptionES : String?
    var strDescriptionPT : String?
    var strDescriptionSE : String?
    var strDescriptionNL : String?
    var strDescriptionHU : String?
    var strDescriptionNO : String?
    var strDescriptionPL : String?
    var strDescriptionIL : String?
    var strFanart1 : String?
    var strFanart2 : String?
    var strFanart3 : String?
    var strFanart4 : String?
    var strBanner : String?
    var strBadge : String?
    var strLogo : String?
    var strPoster : String?
    var strTrophy : String?
    var strNaming : String?
    var strComplete : String?
    var strLocked : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        idLeague <- map["idLeague"]
        idSoccerXML <- map["idSoccerXML"]
        idAPIfootball <- map["idAPIfootball"]
        strSport <- map["strSport"]
        strLeague <- map["strLeague"]
        strLeagueAlternate <- map["strLeagueAlternate"]
        strDivision <- map["strDivision"]
        idCup <- map["idCup"]
        strCurrentSeason <- map["strCurrentSeason"]
        intFormedYear <- map["intFormedYear"]
        dateFirstEvent <- map["dateFirstEvent"]
        strGender <- map["strGender"]
        strCountry <- map["strCountry"]
        strWebsite <- map["strWebsite"]
        strFacebook <- map["strFacebook"]
        strTwitter <- map["strTwitter"]
        strYoutube <- map["strYoutube"]
        strRSS <- map["strRSS"]
        strDescriptionEN <- map["strDescriptionEN"]
        strDescriptionDE <- map["strDescriptionDE"]
        strDescriptionFR <- map["strDescriptionFR"]
        strDescriptionIT <- map["strDescriptionIT"]
        strDescriptionCN <- map["strDescriptionCN"]
        strDescriptionJP <- map["strDescriptionJP"]
        strDescriptionRU <- map["strDescriptionRU"]
        strDescriptionES <- map["strDescriptionES"]
        strDescriptionPT <- map["strDescriptionPT"]
        strDescriptionSE <- map["strDescriptionSE"]
        strDescriptionNL <- map["strDescriptionNL"]
        strDescriptionHU <- map["strDescriptionHU"]
        strDescriptionNO <- map["strDescriptionNO"]
        strDescriptionPL <- map["strDescriptionPL"]
        strDescriptionIL <- map["strDescriptionIL"]
        strFanart1 <- map["strFanart1"]
        strFanart2 <- map["strFanart2"]
        strFanart3 <- map["strFanart3"]
        strFanart4 <- map["strFanart4"]
        strBanner <- map["strBanner"]
        strBadge <- map["strBadge"]
        strLogo <- map["strLogo"]
        strPoster <- map["strPoster"]
        strTrophy <- map["strTrophy"]
        strNaming <- map["strNaming"]
        strComplete <- map["strComplete"]
        strLocked <- map["strLocked"]
    }

}
