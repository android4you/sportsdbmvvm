//
//  LeagueModel.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 01/08/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation
import ObjectMapper

struct LeagueModel : Mappable {
    var idLeague : String?
    var strLeague : String?
    var strSport : String?
    var strLeagueAlternate : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        idLeague <- map["idLeague"]
        strLeague <- map["strLeague"]
        strSport <- map["strSport"]
        strLeagueAlternate <- map["strLeagueAlternate"]
    }

}

