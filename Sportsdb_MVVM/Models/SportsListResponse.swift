//
//  SportsListResponse.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 01/08/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation
import ObjectMapper

//


struct SportsListResponse : Mappable {
    var sports : [SportsModel]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        sports <- map["sports"]
    }

}
