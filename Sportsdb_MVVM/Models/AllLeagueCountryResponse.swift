//
//  AllLeagueCountryResponse.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 03/08/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation


import Foundation
import ObjectMapper

struct AllLeagueCountryResponse : Mappable {
    var countrys : [CountrysModel]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        countrys <- map["countrys"]
    }

}

