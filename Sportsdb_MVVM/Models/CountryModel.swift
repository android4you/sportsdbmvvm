//
//  CountryModel.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 01/08/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation
import ObjectMapper

struct CountryModel : Mappable {
    var name_en : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        name_en <- map["name_en"]
    }

}
