//
//  SportsModel.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 01/08/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation

import Foundation
import ObjectMapper

struct SportsModel : Mappable {
    var idSport : String?
    var strSport : String?
    var strFormat : String?
    var strSportThumb : String?
    var strSportThumbGreen : String?
    var strSportDescription : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        idSport <- map["idSport"]
        strSport <- map["strSport"]
        strFormat <- map["strFormat"]
        strSportThumb <- map["strSportThumb"]
        strSportThumbGreen <- map["strSportThumbGreen"]
        strSportDescription <- map["strSportDescription"]
    }

}

