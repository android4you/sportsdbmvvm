//
//  MenuModel.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 24/04/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

class MenuModel {
    var id : Int?
    var title: String?
    var description : String?
    var image : String?

    init(id: Int, title: String, description: String, image: String) {
             self.id = id
             self.title = title
             self.description = description
             self.image = image
         }
}

