//
//  SeasonsModel.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 02/08/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation
import ObjectMapper

struct SeasonsModel : Mappable {
    var strSeason : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        strSeason <- map["strSeason"]
    }

}
