//
//  LeagueListResponse.swift
//  Sportsdb_MVVM
//
//  Created by Manu Aravind on 01/08/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation
import ObjectMapper

struct LeagueListResponse : Mappable {
    var leagues : [LeagueModel]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        leagues <- map["leagues"]
    }

}
